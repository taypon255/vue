FROM node:12.13.0-alpine
WORKDIR /app/mieruka
RUN apk update && \
    yarn global add @vue/cli && \
    yarn

COPY ./mieruka/package.json .
COPY ./mieruka/yarn.lock .

RUN yarn

CMD ["yarn", "run", "serve"]
