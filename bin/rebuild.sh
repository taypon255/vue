#!/bin/bash
#=============================================
# container rebuild script
#  Execute when adding a package
#=============================================
CWD=`dirname "${0}"`
. ${CWD}/../lib/env_mieruka.sh

sh ${CWD}/stop.sh

# delete container
docker rm ${CONTAINER_ID}
docker rmi ${IMAGE_ID}

# build & start
sh ${CWD}/start.sh
