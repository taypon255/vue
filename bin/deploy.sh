#!/bin/bash
#=============================================
# container deploy script
# $1: local item
#=============================================

CWD=`dirname "${0}"`
. ${CWD}/../lib/env_mieruka.sh
. ${CWD}/../lib/common.sh

[ "${PWD}" = "/c/vue" ] || "echo 'Please Changes directory : [/c/vue]' && exit 1"

if [ -z $1 ]; then
  # if argument is empty
  deploy mieruka ${CONTAINER_ID} /app && echo "Deploy is Succusessfull!!";
else
  deploy $1 ${CONTAINER_ID} /app/$1 && echo "Deploy is Succusessfull!!";
fi
