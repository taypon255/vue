#!/bin/sh
# mieruka start script

CWD=`dirname "${0}"`
. ${CWD}/../lib/env_mieruka.sh
. ${CWD}/../lib/common.sh

ADD_VUETIFY='cd mieruka && yarn add vuetify; sh'
SERVICE_START='cd mieruka && yarn run serve --watch; sh'

#===================================================
# main
#===================================================
container_start ${SERVICE_NAME}
