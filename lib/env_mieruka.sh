#!/bin/bash
# mieruka global env script

PROJECT_NAME="vue"
SERVICE_NAME="app"
VUE_PROJECT_NAME="mieruka"
IMAGE_ID=`docker images | grep ${PROJECT_NAME}_${SERVICE_NAME} | cut -d " " -f 1`
CONTAINER_ID=`docker ps -a| grep ${PROJECT_NAME}_${SERVICE_NAME} | cut -d " " -f 1`
CONTAINER_NAME="${PROJECT_NAME}_${SERVICE_NAME}_1"
SERVICE_ROOT="/app/${PROJECT_NAME}"