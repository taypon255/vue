#!/bin/bash
# common script file

#===================================================
# error logic
# $1: error message
#===================================================
error_message() {
  if [ $? -eq 1 ]; then
    echo ${1}
    exit 1
  fi
}

#===================================================
# build image & container start
# $1: container name
#===================================================
container_start() {
  echo "Start contaier build"
  docker-compose up -d $1 && echo "Succusessful."
}

#===================================================
# deploy
# $1: local item
# $2: container id
# $3: container dir
#===================================================
deploy() {
  echo 'Deploying...'
  docker cp $1 $2:$3 && echo 'Succusessful!!'
}

#===================================================
# send command to container
# $1: container name
# $2: command
#===================================================
send_command() {
  winpty docker-compose exec $1 sh -c "$2"
}