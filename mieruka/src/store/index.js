
import Vue from 'vue'
import Vuex from 'vuex'

Vue.use(Vuex)

export const store = new Vuex.Store({

  state: {
    portfolioUrl: "https://taypon255.gitlab.io/vue_press/",
    tasks: [],
  },

  getters: {
    portfolioUrl: (state) => {
      return state.portfolioUrl
    },
    tasks: (state) => {
      return state.tasks
    },
  },
  mutations: {
    TaskFinished: (state, payload) => {
      state.tasks.forEach(value => {
        if (value.id === payload) {
          if (value.finishedFlag === false) {
            value.finishedFlag = true
            value.status = "Done"
          }
        }
      })
    },
    TaskAdded: (state, payload) => {
      var task = {
        id: 1,
        name: payload,
        status: "Running",
        finishedFlag: false
      }
      if (state.tasks.length !== 0) {
        task.id = state.tasks[state.tasks.length -1].id + 1
      }
      console.log(task)
      state.tasks.push(task)
    },
    TaskClose: (state, payload) => {
      state.tasks.forEach(value => {
        if (value.id === payload) {
          state.tasks.splice(payload, 1)
        }
      })
    }
  },
  actions: {
    TaskFinished: (context, payload) => {
      context.commit('TaskFinished', payload)
    },
    TaskAdded: (context, payload) => {
      context.commit('TaskAdded', payload)
    },
    TaskClose: (context, payload) => {
      context.commit('TaskClose', payload)
    }
  }
})
