import Vue from 'vue'
import VueRouter from 'vue-router'
import Todo from '../views/Todo.vue'
import Register from '../views/Register.vue'
import Test from '../views/Test.vue'
import SkillChartView from '../views/SkillChartView.vue'

Vue.use(VueRouter)

const routes = [
  // {
  //   path: '/todo',
  //   name: 'TodoList',
  //   component: TodoList
  // },
  // {
  //   path: '/todos/add',
  //   name: 'TodoForm',
  //   component: TodoForm
  // },
  {
    path: '/vue',
    name: 'Todo',
    component: Todo
  },
  {
    path: '/regist',
    name: 'Register',
    component: Register
  },
  {
    path: '/test',
    name: 'Test',
    component: Test
  },
  {
    path: '/SkillChart',
    name: 'SkillChartView',
    component: SkillChartView
  }
]

const router = new VueRouter({
  mode: 'history',
  base: process.env.BASE_URL,
  routes
})

export default router
