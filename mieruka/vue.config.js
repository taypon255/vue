module.exports = {
  "devServer": {
    "port": 9997,
    "disableHostCheck": true
  },
  "transpileDependencies": [
    "vuetify"
  ],
  publicPath: "./",
  assetsDir: "",
  outputDir: "dist",
  configureWebpack: {
    devServer: {
      watchOptions: {
        poll: 2000
      }
    }
  }
}