# mieruka

## Project Start up
```
cd /c/vue
docker-compose up -d  #container build & start
docker exec -it app sh  #container sh login
vue create mieruka  #create project
```

```log
/app # vue create mieruka


Vue CLI v4.0.5
? Please pick a preset: Manually select features
? Check the features needed for your project: Babel, Router, Vuex, Linter
? Use history mode for router? (Requires proper server setup for index fallback i
n production) Yes
? Pick a linter / formatter config: Basic
? Pick additional lint features: (Press <space> to select, <a> to toggle all, <i>
 to invert selection)Lint on save
? Where do you prefer placing config for Babel, PostCSS, ESLint, etc.? In package
.json
? Save this as a preset for future projects? No
? Pick the package manager to use when installing dependencies: NPM


Vue CLI v4.0.5
  Creating project in /app/mieruka.
  Installing CLI plugins. This might take a while...


> yorkie@2.0.0 install /app/mieruka/node_modules/yorkie
> node bin/install.js

setting up Git hooks
can't find .git directory, skipping Git hooks installation

> core-js@3.3.3 postinstall /app/mieruka/node_modules/core-js
> node postinstall || echo "ignore"


> core-js-pure@3.3.3 postinstall /app/mieruka/node_modules/core-js-pure
> node postinstall || echo "ignore"

added 1128 packages from 820 contributors in 45.792s
  Invoking generators...
  Installing additional dependencies...

added 58 packages from 44 contributors in 12.081s
  Running completion hooks...

  Generating README.md...

  Successfully created project mieruka.
  Get started with the following commands:
```

## Container commands

```sh
$ cd mieruka
$ vue add vuetify
$ yarn add @mdi/js file-loader material-design-icons-iconfont -D
$ vi src/plugin/vuetify.js  #append: import 'material-design-icons-iconfont/dist/material-design-icons.css'"
$ yarn add vue-chartjs chart.js
$ yarn run serve
```

### Copy items locally
```
cd /c/vue/app
docker cp [ContainorID]:/app/mieruka/. .
```

## How to use
### start(local commands)
```
cd /c/vue
bin/start.sh
```

### start(container command)
```
yarn run serve
```

### deploy(local commands)
bin/deploy.sh [deploy item]


### Customize configuration
See [Configuration Reference](https://cli.vuejs.org/config/).
